/****************************************************************************
*                                                                           *
*   NExtInfo plugin for Kadu                                                *
*   Copyright (C) 2008-2014  Piotr Dąbrowski ultr@ultr.pl                   *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/


#ifndef NEXTINFO_H
	#define NEXTINFO_H


#include <QObject>
#include <QPointer>
#include <QTimer>
#include <QList>

#include "buddies/buddy.h"
#include "configuration/configuration-aware-object.h"
#include "gui/actions/action-description.h"
#include "gui/widgets/buddy-configuration-widget-factory.h"
#include "gui/windows/main-configuration-window.h"
#include "notify/notify-event.h"
#include "plugin/plugin-root-component.h"

#include "defines.h"
#include "extendedinformationwidgetfactory.h"
#include "noteswidgetfactory.h"


enum RemindTime
{
	RemindTimeNow,
	RemindTimeTomorrow,
	RemindTimeTheDay,
	RemindTimeNextYear
};


class NExtInfo : public ConfigurationUiHandler, public ConfigurationAwareObject, public BuddyConfigurationWidgetFactory, public PluginRootComponent
{
	Q_OBJECT
	Q_INTERFACES( PluginRootComponent )
	Q_PLUGIN_METADATA( IID "im.kadu.PluginRootComponent" )
	public:
		virtual bool init( bool firstLoad );
		virtual void done();
		NExtInfo();
		~NExtInfo();
	public:
		static void updateActionBirthday( Action *action );
		static void updateActionNameday( Action *action );
		static void updateActionBirthdayMenu( Action *action );
		static void updateActionNamedayMenu( Action *action);
	public:
		static QPair< bool, QPair<int,int> > checkBirthdayNotify( Buddy buddy );
		static QPair< bool, QPair<int,int> > checkNamedayNotify(  Buddy buddy );
		static bool checkBirthdayRemind( Buddy buddy );
		static bool checkNamedayRemind(  Buddy buddy );
	public:
		virtual BuddyConfigurationWidget *createWidget( const Buddy &buddy, QWidget *parent );
		virtual void mainConfigurationWindowCreated( MainConfigurationWindow *mainConfigurationWindow );
		void updateActionsBirthday();
		void updateActionsNameday();
	public slots:
		void actionBirthdayCreated( Action *action );
		void actionNamedayCreated(  Action *action );
		void actionBirthdayTriggered( QAction *sender, bool checked );
		void actionNamedayTriggered(  QAction *sender, bool checked );
		void actionBirthdayNowTriggered();
		void actionBirthdayTomorrowTriggered();
		void actionBirthdayTheDayTriggered();
		void actionBirthdayNextYearTriggered();
		void actionNamedayNowTriggered();
		void actionNamedayTomorrowTriggered();
		void actionNamedayTheDayTriggered();
		void actionNamedayNextYearTriggered();
		void showHelp();
		void importDataFromExtInfo();
	protected:
		virtual void configurationUpdated();
	private slots:
		void notifyBirthdayNameday();
	private:
		void createDefaultConfiguration();
		void setBirthdayRemind( Buddy buddy, RemindTime time );
		void setNamedayRemind( Buddy buddy, RemindTime time );
		void importOldData( int olddataformatversion );
		QString ordinal( QString code, int n );
		ExtendedInformationWidgetFactory *extendedinformationwidgetfactory;
		NotesWidgetFactory *noteswidgetfactory;
		ActionDescription *actionbirthday, *actionnameday;
		QTimer *birthdaynamedaytimer;
		ActionDescription *nextinfoaction;
		NotifyEvent *notifyevent;
};


#endif
