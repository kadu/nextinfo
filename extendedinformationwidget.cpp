/****************************************************************************
*                                                                           *
*   NExtInfo plugin for Kadu                                                *
*   Copyright (C) 2014  Piotr Dąbrowski ultr@ultr.pl                        *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/




#include "extendedinformationwidget.h"

#include <QCoreApplication>

#include "buddynextinfodata.h"




ExtendedInformationWidget::ExtendedInformationWidget(const Buddy &buddy, QWidget *parent) :
	BuddyConfigurationWidget( buddy, parent ),
	StateNotifier( new SimpleConfigurationValueStateNotifier( this ) )
{
	setWindowTitle( tr( "Extended Information" ) );
	createGui();
	configurationUpdated();
	loadValues();
}


ExtendedInformationWidget::~ExtendedInformationWidget()
{
}


void ExtendedInformationWidget::createGui()
{
	layout_extinfotab = new QVBoxLayout( this );
	layout_extinfotab->setMargin( 5 );
	layout_extinfotab->setSpacing( 3 );

	extinfotab_separator1 = new QWidget( this );
		extinfotab_separator1->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator1->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator1 );

	layout_firstname = new QHBoxLayout( 0 );
	layout_firstname->setMargin( 0 );
	layout_firstname->setSpacing( 6 );
		label_firstname = new QLabel( this );
		label_firstname->setAlignment( Qt::AlignRight | Qt::AlignVCenter | Qt::AlignVCenter | Qt::AlignVCenter );
		layout_firstname->addWidget( label_firstname );
		field_firstname = new QLineEdit( this );
		layout_firstname->addWidget( field_firstname );
		layout_extinfotab->addLayout( layout_firstname );
	layout_middlename = new QHBoxLayout( 0 );
	layout_middlename->setMargin( 0 );
	layout_middlename->setSpacing( 6 );
		label_middlename = new QLabel( this );
		label_middlename->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_middlename->addWidget( label_middlename );
		field_middlename = new QLineEdit( this );
		layout_middlename->addWidget( field_middlename );
		layout_extinfotab->addLayout( layout_middlename );
	layout_lastname = new QHBoxLayout( 0 );
	layout_lastname->setMargin( 0 );
	layout_lastname->setSpacing( 6 );
		label_lastname = new QLabel( this );
		label_lastname->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_lastname->addWidget( label_lastname );
		field_lastname = new QLineEdit( this );
		layout_lastname->addWidget( field_lastname );
		layout_extinfotab->addLayout( layout_lastname );
	layout_nickname = new QHBoxLayout( 0 );
	layout_nickname->setMargin( 0 );
	layout_nickname->setSpacing( 6 );
		label_nickname = new QLabel( this );
		label_nickname->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_nickname->addWidget( label_nickname );
		field_nickname = new QLineEdit( this );
		layout_nickname->addWidget( field_nickname );
		layout_extinfotab->addLayout( layout_nickname );

	extinfotab_separator2 = new QWidget( this );
		extinfotab_separator2->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator2->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator2 );

	layout_gender = new QHBoxLayout( 0 );
	layout_gender->setMargin( 0 );
	layout_gender->setSpacing( 6 );
		label_gender = new QLabel( this );
		label_gender->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_gender->addWidget( label_gender );
		field_gender = new QComboBox( this );
		field_gender->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
		layout_gender->addWidget( field_gender );
		layout_extinfotab->addLayout( layout_gender );

	extinfotab_separator3 = new QWidget( this );
		extinfotab_separator3->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator3->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator3 );

	layout_address = new QHBoxLayout( 0 );
	layout_address->setMargin( 0 );
	layout_address->setSpacing( 6 );
		label_address = new QLabel( this );
		label_address->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_address->addWidget( label_address );
		field_address = new QLineEdit( this );
		layout_address->addWidget( field_address );
		layout_extinfotab->addLayout( layout_address );
	layout_city = new QHBoxLayout( 0 );
	layout_city->setMargin( 0 );
	layout_city->setSpacing( 6 );
		label_city = new QLabel( this );
		label_city->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_city->addWidget( label_city );
		field_city = new QLineEdit( this );
		layout_city->addWidget( field_city );
		layout_extinfotab->addLayout( layout_city );

	extinfotab_separator4 = new QWidget( this );
		extinfotab_separator4->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator4->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator4 );

	layout_phone = new QHBoxLayout( 0 );
	layout_phone->setMargin( 0 );
	layout_phone->setSpacing( 6 );
		label_phone = new QLabel( this );
		label_phone->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_phone->addWidget( label_phone );
		field_phone = new QLineEdit( this );
		layout_phone->addWidget( field_phone );
		layout_extinfotab->addLayout( layout_phone );
	layout_mobile = new QHBoxLayout( 0 );
	layout_mobile->setMargin( 0 );
	layout_mobile->setSpacing( 6 );
		label_mobile = new QLabel( this );
		label_mobile->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_mobile->addWidget( label_mobile );
		field_mobile = new QLineEdit( this );
		layout_mobile->addWidget( field_mobile );
		layout_extinfotab->addLayout( layout_mobile );

	extinfotab_separator5 = new QWidget( this );
		extinfotab_separator5->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator5->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator5 );

	layout_email = new QHBoxLayout( 0 );
	layout_email->setMargin( 0 );
	layout_email->setSpacing( 6 );
		label_email = new QLabel( this );
		label_email->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_email->addWidget( label_email );
		field_email = new QLineEdit( this );
		layout_email->addWidget( field_email );
		layout_extinfotab->addLayout( layout_email );
	layout_email2 = new QHBoxLayout( 0 );
	layout_email2->setMargin( 0 );
	layout_email2->setSpacing( 6 );
		label_email2 = new QLabel( this );
		label_email2->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_email2->addWidget( label_email2 );
		field_email2 = new QLineEdit( this );
		layout_email2->addWidget( field_email2 );
		layout_extinfotab->addLayout( layout_email2 );
	layout_www = new QHBoxLayout( 0 );
	layout_www->setMargin( 0 );
	layout_www->setSpacing( 6 );
		label_www = new QLabel( this );
		label_www->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_www->addWidget( label_www );
		field_www = new QLineEdit( this );
		layout_www->addWidget( field_www );
		layout_extinfotab->addLayout( layout_www );

	extinfotab_separator6 = new QWidget( this );
		extinfotab_separator6->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator6->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator6 );

	layout_birthday = new QHBoxLayout( 0 );
	layout_birthday->setMargin( 0 );
	layout_birthday->setSpacing( 6 );
		label_birthday = new QLabel( this );
		label_birthday->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_birthday->addWidget( label_birthday );
		field_birthday = new QLineEdit( this );
		layout_birthday->addWidget( field_birthday );
		layout_extinfotab->addLayout( layout_birthday );
	layout_nameday = new QHBoxLayout( 0 );
	layout_nameday->setMargin( 0 );
	layout_nameday->setSpacing( 6 );
		label_nameday = new QLabel( this );
		label_nameday->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
		layout_nameday->addWidget( label_nameday );
		field_nameday = new QLineEdit( this );
		layout_nameday->addWidget( field_nameday );
		layout_extinfotab->addLayout( layout_nameday );

	extinfotab_separator7 = new QWidget( this );
		extinfotab_separator7->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		extinfotab_separator7->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_extinfotab->addWidget( extinfotab_separator7 );

	extinfotab_spacer = new QSpacerItem( 20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding );
		layout_extinfotab->addItem( extinfotab_spacer );

	updateWidgets();
}


void ExtendedInformationWidget::updateWidgets()
{
	// labels: set texts
	label_firstname->setText(  QCoreApplication::translate( "@nextinfo", "First name"            ) + ":" );
	label_middlename->setText( QCoreApplication::translate( "@nextinfo", "Middle name"           ) + ":" );
	label_lastname->setText(   QCoreApplication::translate( "@nextinfo", "Last name"             ) + ":" );
	label_nickname->setText(   QCoreApplication::translate( "@nextinfo", "Nickname"              ) + ":" );
	label_gender->setText(     QCoreApplication::translate( "@nextinfo", "Gender"                ) + ":" );
	label_address->setText(    QCoreApplication::translate( "@nextinfo", "Address"               ) + ":" );
	label_city->setText(       QCoreApplication::translate( "@nextinfo", "City"                  ) + ":" );
	label_phone->setText(      QCoreApplication::translate( "@nextinfo", "Phone"                 ) + ":" );
	label_mobile->setText(     QCoreApplication::translate( "@nextinfo", "Mobile"                ) + ":" );
	label_email->setText(      QCoreApplication::translate( "@nextinfo", "e-mail"                ) + ":" );
	label_email2->setText(     QCoreApplication::translate( "@nextinfo", "Alternative e-mail"    ) + ":" );
	label_www->setText(        QCoreApplication::translate( "@nextinfo", "Website"               ) + ":" );
	label_birthday->setText(   QCoreApplication::translate( "@nextinfo", "Birthday (DD.MM.YYYY)" ) + ":" );
	label_nameday->setText(    QCoreApplication::translate( "@nextinfo", "Name-day (DD.MM)"      ) + ":" );
	// set gender combobox items
	field_gender->addItem( ""                                                  , 0 );
	field_gender->addItem( QCoreApplication::translate( "@nextinfo", "female" ), 1 );
	field_gender->addItem( QCoreApplication::translate( "@nextinfo", "male"   ), 2 );
	// labels: set new sizes
	label_firstname->adjustSize();
	label_middlename->adjustSize();
	label_lastname->adjustSize();
	label_nickname->adjustSize();
	label_gender->adjustSize();
	label_address->adjustSize();
	label_city->adjustSize();
	label_phone->adjustSize();
	label_mobile->adjustSize();
	label_email->adjustSize();
	label_email2->adjustSize();
	label_www->adjustSize();
	label_birthday->adjustSize();
	label_nameday->adjustSize();
	// labels: get maximum width
	int labels_width = 1;  // px
	if( label_firstname->width()  > labels_width ) { labels_width = label_firstname->width();  }
	if( label_middlename->width() > labels_width ) { labels_width = label_middlename->width(); }
	if( label_lastname->width()   > labels_width ) { labels_width = label_lastname->width();   }
	if( label_nickname->width()   > labels_width ) { labels_width = label_nickname->width();   }
	if( label_gender->width()     > labels_width ) { labels_width = label_gender->width();     }
	if( label_address->width()    > labels_width ) { labels_width = label_address->width();    }
	if( label_city->width()       > labels_width ) { labels_width = label_city->width();       }
	if( label_phone->width()      > labels_width ) { labels_width = label_phone->width();      }
	if( label_mobile->width()     > labels_width ) { labels_width = label_mobile->width();     }
	if( label_email->width()      > labels_width ) { labels_width = label_email->width();      }
	if( label_email2->width()     > labels_width ) { labels_width = label_email2->width();     }
	if( label_www->width()        > labels_width ) { labels_width = label_www->width();        }
	if( label_birthday->width()   > labels_width ) { labels_width = label_birthday->width();   }
	if( label_nameday->width()    > labels_width ) { labels_width = label_nameday->width();    }
	// labels: set width of each label to the maximum width
	label_firstname->setMinimumWidth(  labels_width );
	label_middlename->setMinimumWidth( labels_width );
	label_lastname->setMinimumWidth(   labels_width );
	label_nickname->setMinimumWidth(   labels_width );
	label_gender->setMinimumWidth(     labels_width );
	label_address->setMinimumWidth(    labels_width );
	label_city->setMinimumWidth(       labels_width );
	label_phone->setMinimumWidth(      labels_width );
	label_mobile->setMinimumWidth(     labels_width );
	label_email->setMinimumWidth(      labels_width );
	label_email2->setMinimumWidth(     labels_width );
	label_www->setMinimumWidth(        labels_width );
	label_birthday->setMinimumWidth(   labels_width );
	label_nameday->setMinimumWidth(    labels_width );
	// labels: update labels' geometry
	label_firstname->updateGeometry();
	label_middlename->updateGeometry();
	label_lastname->updateGeometry();
	label_nickname->updateGeometry();
	label_gender->updateGeometry();
	label_address->updateGeometry();
	label_city->updateGeometry();
	label_phone->updateGeometry();
	label_mobile->updateGeometry();
	label_email->updateGeometry();
	label_email2->updateGeometry();
	label_www->updateGeometry();
	label_birthday->updateGeometry();
	label_nameday->updateGeometry();
	// read only
	QFont labelfont = label_phone->font();
	labelfont.setItalic( true );
	QFont fieldfont = field_phone->font();
	fieldfont.setItalic( true );
	field_phone->setReadOnly( true );
	field_phone->setFont( fieldfont );
	label_phone->setFont( labelfont );
	field_mobile->setReadOnly( true );
	field_mobile->setFont( fieldfont );
	label_mobile->setFont( labelfont );
	field_email->setReadOnly( true );
	field_email->setFont( fieldfont );
	label_email->setFont( labelfont );
	field_www->setReadOnly( true );
	field_www->setFont( fieldfont );
	label_www->setFont( labelfont );
}


void ExtendedInformationWidget::configurationUpdated()
{
}


void ExtendedInformationWidget::loadValues()
{
	// load standard information
	field_firstname->setText(  buddy().firstName() );
	field_middlename->setText( BuddyNExtInfoData::middleName( buddy() ) );
	field_lastname->setText(   buddy().lastName()  );
	field_nickname->setText(   buddy().nickName()  );
	// load standard read-only information
	field_phone->setText(      buddy().homePhone() );
	field_mobile->setText(     buddy().mobile()    );
	field_email->setText(      buddy().email()     );
	field_www->setText(        buddy().website()   );
	// load gender
	field_gender->setCurrentIndex( buddy().gender() );
	// load extended information
	field_address->setText(    BuddyNExtInfoData::address(   buddy() ) );
	field_city->setText(       BuddyNExtInfoData::city(      buddy() ) );
	field_email2->setText(     BuddyNExtInfoData::email2(    buddy() ) );
	field_birthday->setText(   BuddyNExtInfoData::birthday(  buddy() ) );
	field_nameday->setText(    BuddyNExtInfoData::nameday(   buddy() ) );
	// set state notifier
	StateNotifier->setState(StateNotChanged);
}


void ExtendedInformationWidget::updateState()
{
	StateNotifier->setState( StateChangedDataValid );
}


const ConfigurationValueStateNotifier * ExtendedInformationWidget::stateNotifier() const
{
	return StateNotifier;
}


void ExtendedInformationWidget::apply()
{
	// check birthday format
	if( ! field_birthday->text().contains( QRegExp( NEXTINFO_REGEXPBIRTHDAY ) ) )  // if bad format
		field_birthday->setText( "" );
	// check name-day format
	if( ! field_nameday->text().contains( QRegExp( NEXTINFO_REGEXPNAMEDAY ) ) )  // if bad format
		field_nameday->setText( "" );
	// save standard information
	buddy().setFirstName( field_firstname->text() );
	buddy().setLastName(  field_lastname->text()  );
	buddy().setNickName(  field_nickname->text()  );
	// save gender
	int gender = field_gender->currentIndex();
	if( ( gender < 0 ) || ( gender > 2 ) )
		gender = 0;
	buddy().setGender( (BuddyGender)gender );
	// save extended information
	BuddyNExtInfoData::setMiddleName( buddy(), field_middlename->text()       );
	BuddyNExtInfoData::setAddress(    buddy(), field_address->text()          );
	BuddyNExtInfoData::setCity(       buddy(), field_city->text()             );
	BuddyNExtInfoData::setEmail2(     buddy(), field_email2->text()           );
	BuddyNExtInfoData::setBirthday(   buddy(), field_birthday->text()         );
	BuddyNExtInfoData::setNameday(    buddy(), field_nameday->text()          );
	// set state notifier
	StateNotifier->setState(StateNotChanged);
	// reload values later (ex. to update standard information)
	QMetaObject::invokeMethod( this, "loadValues", Qt::QueuedConnection );
}


void ExtendedInformationWidget::cancel()
{
	loadValues();
}




#include "moc_extendedinformationwidget.cpp"
