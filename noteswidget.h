/****************************************************************************
*                                                                           *
*   NExtInfo plugin for Kadu                                                *
*   Copyright (C) 2014  Piotr Dąbrowski ultr@ultr.pl                        *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/


#ifndef NOTES_WIDGET_H
	#define NOTES_WIDGET_H


#undef None

#include <QComboBox>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QObject>
#include <QPointer>
#include <QPushButton>
#include <QScrollArea>
#include <QTabWidget>
#include <QTextEdit>
#include <QVariant>
#include <QWidget>

#include "configuration/configuration-aware-object.h"
#include "gui/widgets/buddy-configuration-widget.h"
#include "gui/widgets/simple-configuration-value-state-notifier.h"

#include "defines.h"


class NotesWidget : public BuddyConfigurationWidget, ConfigurationAwareObject
{
	Q_OBJECT
	public:
		explicit NotesWidget(const Buddy &buddy, QWidget *parent = nullptr);
		virtual ~NotesWidget();
		virtual const ConfigurationValueStateNotifier *stateNotifier() const;
		virtual void apply();
		virtual void cancel();
	protected:
		virtual void configurationUpdated();
	private slots:
		void updateState();
		void updateWidgets();
		void loadValues();
	private:
		SimpleConfigurationValueStateNotifier *StateNotifier;
		void createGui();
		QVBoxLayout* layout_notestab;
		QWidget* notestab_separator1;
		QLabel* label_interests; QTextEdit* field_interests;
		QWidget* notestab_separator2;
		QLabel* label_notes;     QTextEdit* field_notes;
		QWidget* notestab_separator3;
};


#endif
