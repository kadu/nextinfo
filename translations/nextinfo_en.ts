<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Buddies</source>
        <translation>Buddies</translation>
    </message>
    <message>
        <source>Extended information</source>
        <translation>Extended information</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>Enable notifications</source>
        <translation>Enable notifications</translation>
    </message>
    <message>
        <source>Notification advance</source>
        <translation>Notification advance</translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translatorcomment>Notification advance: %n day(s)</translatorcomment>
        <translation>
            <numerusform>%n day</numerusform>
            <numerusform>%n days</numerusform>
        </translation>
    </message>
    <message>
        <source>Delay between notifications</source>
        <translation>Delay between notifications</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n second</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Notify about birthdays</source>
        <translation>Notify about birthdays</translation>
    </message>
    <message>
        <source>Notify about name-days</source>
        <translation>Notify about name-days</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>Show help</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Import data from ext_info module</source>
        <translation>Import data from ext_info module</translation>
    </message>
    <message>
        <source>Birthday/name-day notification</source>
        <translation>Birthday/name-day notification</translation>
    </message>
</context>
<context>
    <name>@nextinfo</name>
    <message>
        <source>Extended information</source>
        <translation>Extended information</translation>
    </message>
    <message>
        <source>Birthday notification</source>
        <translation>Birthday notification</translation>
    </message>
    <message>
        <source>Name-day notification</source>
        <translation>Name-day notification</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Nickname</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Address</translation>
    </message>
    <message>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Gender</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobile</translation>
    </message>
    <message>
        <source>female</source>
        <translation>female</translation>
    </message>
    <message>
        <source>male</source>
        <translation>male</translation>
    </message>
    <message>
        <source>e-mail</source>
        <translation>e-mail</translation>
    </message>
    <message>
        <source>Alternative e-mail</source>
        <translation>Alternative e-mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Website</translation>
    </message>
    <message>
        <source>Birthday (DD.MM.YYYY)</source>
        <translation>Birthday (DD.MM.YYYY)</translation>
    </message>
    <message>
        <source>Name-day (DD.MM)</source>
        <translation>Name-day (DD.MM)</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Interests</source>
        <translation>Interests</translation>
    </message>
    <message>
        <source>These tags are recognised by the Kadu&apos;s parser:</source>
        <translation>These tags are recognised by the Kadu&apos;s parser:</translation>
    </message>
    <message>
        <source>Birthday notifications</source>
        <translation>Birthday notifications</translation>
    </message>
    <message>
        <source>Name-day notifications</source>
        <translation>Name-day notifications</translation>
    </message>
    <message>
        <source>Keep reminding me now</source>
        <translation>Keep reminding me now</translation>
    </message>
    <message>
        <source>Remind me tomorrow</source>
        <translation>Remind me tomorrow</translation>
    </message>
    <message>
        <source>Remind me on the day</source>
        <translation>Remind me on the day</translation>
    </message>
    <message>
        <source>Remind me next year</source>
        <translation>Remind me next year</translation>
    </message>
    <message>
        <source>code(n-th birthday)</source>
        <translatorcomment>JavaScript code returning localized ordinal number for n. In English translation: &apos;1st&apos; for n=1, &apos;2nd&apos; for n=2, etc.</translatorcomment>
        <translation>var s = &quot;&quot;;
if( n &lt;= 0 )
	s = &quot;.&quot;;
else if( n == 1 )
	s = &quot;st&quot;;
else if( n == 2 )
	s = &quot;nd&quot;;
else if( n == 3 )
	s = &quot;rd&quot;;
else if( n &lt; 20 )
	s = &quot;th&quot;;
else if( n % 10 == 1 )
	s = &quot;st&quot;;
else if( n % 10 == 2 )
	s = &quot;nd&quot;;
else if( n % 10 == 3 )
	s = &quot;rd&quot;;
else
	s = &quot;th&quot;;
return &quot;&quot; + n + s;</translation>
    </message>
    <message>
        <source>Select ext_info data file to import</source>
        <translation>Select ext_info data file to import</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
    <message>
        <source>Tlen</source>
        <translation>Tlen</translation>
    </message>
    <message>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <source>ICQ</source>
        <translation>ICQ</translation>
    </message>
    <message numerus="yes">
        <source>%n contact(s) imported.</source>
        <translation>
            <numerusform>%n contact imported.</numerusform>
            <numerusform>%n contacts imported.</numerusform>
        </translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Data import</source>
        <translation>Data import</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Birthday</translation>
    </message>
    <message>
        <source>Name-day</source>
        <translation>Name-day</translation>
    </message>
    <message>
        <source>Middle name</source>
        <translation>Middle name</translation>
    </message>
</context>
<context>
    <name>ExtendedInformationWidget</name>
    <message>
        <source>Extended Information</source>
        <translation>Extended information</translation>
    </message>
</context>
<context>
    <name>NotesWidget</name>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
</context>
</TS>
