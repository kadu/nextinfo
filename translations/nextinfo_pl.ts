<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Buddies</source>
        <translation>Znajomi</translation>
    </message>
    <message>
        <source>Extended information</source>
        <translation>Rozszerzone informacje</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Powiadomienia</translation>
    </message>
    <message>
        <source>Enable notifications</source>
        <translation>Włącz powiadomienia</translation>
    </message>
    <message>
        <source>Notification advance</source>
        <translation>Powiadamiaj z wyprzedzeniem</translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translatorcomment>Notification advance: %n day(s)</translatorcomment>
        <translation>
            <numerusform>%n dnia</numerusform>
            <numerusform>%n dni</numerusform>
            <numerusform>%n dni</numerusform>
        </translation>
    </message>
    <message>
        <source>Delay between notifications</source>
        <translation>Odstęp między powiadomieniami</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Notify about birthdays</source>
        <translation>Powiadamiaj o urodzinach</translation>
    </message>
    <message>
        <source>Notify about name-days</source>
        <translation>Powiadamiaj o imieninach</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>Pokaż pomoc</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Dane</translation>
    </message>
    <message>
        <source>Import data from ext_info module</source>
        <translation>Importuj dane z modułu ext_info</translation>
    </message>
    <message>
        <source>Birthday/name-day notification</source>
        <translation>Powiadomienia o urodzinach/imieninach</translation>
    </message>
</context>
<context>
    <name>@nextinfo</name>
    <message>
        <source>Extended information</source>
        <translation>Rozszerzone informacje</translation>
    </message>
    <message>
        <source>Birthday notification</source>
        <translation>Powiadomienie o urodzinach</translation>
    </message>
    <message>
        <source>Name-day notification</source>
        <translation>Powiadomienie o imieninach</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonim</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Miasto</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Telefon komórkowy</translation>
    </message>
    <message>
        <source>female</source>
        <translation>kobieta</translation>
    </message>
    <message>
        <source>male</source>
        <translation>mężczyzna</translation>
    </message>
    <message>
        <source>e-mail</source>
        <translation>Adres e-mail</translation>
    </message>
    <message>
        <source>Alternative e-mail</source>
        <translation>Alternatywny adres e-mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Strona WWW</translation>
    </message>
    <message>
        <source>Birthday (DD.MM.YYYY)</source>
        <translation>Urodziny (DD.MM.RRRR)</translation>
    </message>
    <message>
        <source>Name-day (DD.MM)</source>
        <translation>Imieniny (DD.MM)</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notatki</translation>
    </message>
    <message>
        <source>Interests</source>
        <translation>Zainteresowania</translation>
    </message>
    <message>
        <source>These tags are recognised by the Kadu&apos;s parser:</source>
        <translation>Te tagi są rozpoznawane przez parser Kadu:</translation>
    </message>
    <message>
        <source>Birthday notifications</source>
        <translation>Powiadomienia o urodzinach</translation>
    </message>
    <message>
        <source>Name-day notifications</source>
        <translation>Powiadomienia o imieninach</translation>
    </message>
    <message>
        <source>Keep reminding me now</source>
        <translation>Przypominaj teraz</translation>
    </message>
    <message>
        <source>Remind me tomorrow</source>
        <translation>Przypomnij jutro</translation>
    </message>
    <message>
        <source>Remind me on the day</source>
        <translation>Przypomnij w tym dniu</translation>
    </message>
    <message>
        <source>Remind me next year</source>
        <translation>Przypomij za rok</translation>
    </message>
    <message>
        <source>code(n-th birthday)</source>
        <translatorcomment>JavaScript code returning localized ordinal number for n. In English translation: &apos;1st&apos; for n=1, &apos;2nd&apos; for n=2, etc.</translatorcomment>
        <translation>var s = &quot;&quot;;
if( n &lt; 0 )
	s = &quot;.&quot;;
else if( n == 0 )
	s = &quot;-we&quot;;
else if( n &gt; 10 &amp;&amp; n &lt; 20 )
	s = &quot;-te&quot;;
else if( n % 10 == 1 )
	s = &quot;-sze&quot;;
else if( n % 10 == 2 )
	s = &quot;-gie&quot;;
else if( n % 10 == 3 )
	s = &quot;-cie&quot;;
else if( n % 10 == 7 )
	s = &quot;-me&quot;;
else if( n % 10 == 8 )
	s = &quot;-me&quot;;
else
	s = &quot;-te&quot;;
return &quot;&quot; + n + s;</translation>
    </message>
    <message>
        <source>Select ext_info data file to import</source>
        <translation>Wybierz plik z danymi ext_info do zaimportowania</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>e-mail</translation>
    </message>
    <message>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
    <message>
        <source>Tlen</source>
        <translation>Tlen</translation>
    </message>
    <message>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <source>ICQ</source>
        <translation>ICQ</translation>
    </message>
    <message numerus="yes">
        <source>%n contact(s) imported.</source>
        <translation>
            <numerusform>Zaimportowano %n kontakt.</numerusform>
            <numerusform>Zaimportowano %n kontakty.</numerusform>
            <numerusform>Zaimportowano %n kontaktów.</numerusform>
        </translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; swoje &lt;b&gt;%2 urodziny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi swoje &lt;b&gt;%2 urodziny&lt;/b&gt; za &lt;b&gt;%3 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi swoje &lt;b&gt;%2 urodziny&lt;/b&gt; za &lt;b&gt;%3 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi swoje &lt;b&gt;%2 urodziny&lt;/b&gt; za &lt;b&gt;%3 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;dzisiaj&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;jutro&lt;/b&gt; &lt;b&gt;imieniny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;imieniny&lt;/b&gt; za &lt;b&gt;%2 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;imieniny&lt;/b&gt; za &lt;b&gt;%2 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; obchodzi &lt;b&gt;imieniny&lt;/b&gt; za &lt;b&gt;%2 dni&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Data import</source>
        <translation>Import danych</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Urodziny</translation>
    </message>
    <message>
        <source>Name-day</source>
        <translation>Imieniny</translation>
    </message>
    <message>
        <source>Middle name</source>
        <translation>Drugie imię</translation>
    </message>
</context>
<context>
    <name>ExtendedInformationWidget</name>
    <message>
        <source>Extended Information</source>
        <translation>Rozszerzone informacje</translation>
    </message>
</context>
<context>
    <name>NotesWidget</name>
    <message>
        <source>Notes</source>
        <translation>Notatki</translation>
    </message>
</context>
</TS>
