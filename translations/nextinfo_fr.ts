<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Buddies</source>
        <translation>Amis</translation>
    </message>
    <message>
        <source>Extended information</source>
        <translation>Informations étendues</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>Enable notifications</source>
        <translation>Activer les notifications</translation>
    </message>
    <message>
        <source>Notification advance</source>
        <translation>Notification avancée</translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translatorcomment>Notification advance: %n day(s)</translatorcomment>
        <translation>
            <numerusform>%n jour</numerusform>
            <numerusform>%n jours</numerusform>
        </translation>
    </message>
    <message>
        <source>Delay between notifications</source>
        <translation>Intervalle entre les notifications</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n seconde</numerusform>
            <numerusform>%n secondes</numerusform>
        </translation>
    </message>
    <message>
        <source>Notify about birthdays</source>
        <translation>Signaler les anniversaires</translation>
    </message>
    <message>
        <source>Notify about name-days</source>
        <translation>Signaler les fêtes  patronales</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>Afficher l&apos;aide</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>Import data from ext_info module</source>
        <translation>Importer des données depuis un module ext_info</translation>
    </message>
    <message>
        <source>Birthday/name-day notification</source>
        <translation>Notification d&apos;anniversaire / de fête patronale</translation>
    </message>
</context>
<context>
    <name>@nextinfo</name>
    <message>
        <source>Extended information</source>
        <translation>Informations étendues</translation>
    </message>
    <message>
        <source>Birthday notification</source>
        <translation>Notification d&apos;anniversaire</translation>
    </message>
    <message>
        <source>Name-day notification</source>
        <translation>Notification de fête patronale</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Sexe</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <source>female</source>
        <translation>femme</translation>
    </message>
    <message>
        <source>male</source>
        <translation>homme</translation>
    </message>
    <message>
        <source>e-mail</source>
        <translation>email</translation>
    </message>
    <message>
        <source>Alternative e-mail</source>
        <translation>Second email</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <source>Birthday (DD.MM.YYYY)</source>
        <translation>Date de naissance (DD-MM-YYYY)</translation>
    </message>
    <message>
        <source>Name-day (DD.MM)</source>
        <translation>Fête patronale (DD-MM)</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
    <message>
        <source>Interests</source>
        <translation>Centres d&apos;intérêts</translation>
    </message>
    <message>
        <source>These tags are recognised by the Kadu&apos;s parser:</source>
        <translation>Ces mots-clefs sont reconnus bar l&apos;analyseur de Kadu :</translation>
    </message>
    <message>
        <source>Birthday notifications</source>
        <translation>Notifications d&apos;anniversaires</translation>
    </message>
    <message>
        <source>Name-day notifications</source>
        <translation>Notification de fêtes patronales</translation>
    </message>
    <message>
        <source>Keep reminding me now</source>
        <translation>Rappeler plus tard</translation>
    </message>
    <message>
        <source>Remind me tomorrow</source>
        <translation>Rappeler demain</translation>
    </message>
    <message>
        <source>Remind me on the day</source>
        <translation>Rappeler le jour</translation>
    </message>
    <message>
        <source>Remind me next year</source>
        <translation>Rappeler l&apos;année prochaine</translation>
    </message>
    <message>
        <source>code(n-th birthday)</source>
        <translatorcomment>JavaScript code returning localized ordinal number for n. In English translation: &apos;1st&apos; for n=1, &apos;2nd&apos; for n=2, etc.</translatorcomment>
        <translation>var s = &quot;&quot;;
if( n &lt;= 0 )
	s = &quot;.&quot;;
else if( n == 1 )
	s = &quot;er&quot;;
else
	s = &quot;ème&quot;;
return &quot;&quot; + n + s;</translation>
    </message>
    <message>
        <source>Select ext_info data file to import</source>
        <translation>Sélectionner les données du fichier ext-info à importer</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
    <message>
        <source>Tlen</source>
        <translation>Tlen</translation>
    </message>
    <message>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <source>ICQ</source>
        <translation>ICQ</translation>
    </message>
    <message numerus="yes">
        <source>%n contact(s) imported.</source>
        <translation>
            <numerusform>%n contact importé.</numerusform>
            <numerusform>%n contacts importés.</numerusform>
        </translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; dans &lt;b&gt;%3 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; dans &lt;b&gt;%3 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; fête son &lt;b&gt;%2 anniversaire&lt;/b&gt; dans &lt;b&gt;%3 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;aujourd&apos;hui&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; &lt;b&gt;demain&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; dans &lt;b&gt;%2 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; dans &lt;b&gt;%2 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; a sa &lt;b&gt;fête&lt;/b&gt; dans &lt;b&gt;%2 jours&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Data import</source>
        <translation>Importation de données</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <source>Name-day</source>
        <translation>Fête patronale</translation>
    </message>
    <message>
        <source>Middle name</source>
        <translation>Second nom</translation>
    </message>
</context>
<context>
    <name>ExtendedInformationWidget</name>
    <message>
        <source>Extended Information</source>
        <translation>Informations étendues</translation>
    </message>
</context>
<context>
    <name>NotesWidget</name>
    <message>
        <source>Notes</source>
        <translation>Remarques</translation>
    </message>
</context>
</TS>
