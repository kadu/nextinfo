<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Buddies</source>
        <translation>Kamarádi</translation>
    </message>
    <message>
        <source>Extended information</source>
        <translation>Rozšířené informace</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <source>Enable notifications</source>
        <translation>Povolit oznámení</translation>
    </message>
    <message>
        <source>Notification advance</source>
        <translation>Předstih oznámení</translation>
    </message>
    <message numerus="yes">
        <source>%n day(s)</source>
        <translatorcomment>Notification advance: %n day(s)</translatorcomment>
        <translation>
            <numerusform>%n den</numerusform>
            <numerusform>%n dny</numerusform>
            <numerusform>%n dnů</numerusform>
        </translation>
    </message>
    <message>
        <source>Delay between notifications</source>
        <translation>Zpoždění mezi oznámeními</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n sekunda</numerusform>
            <numerusform>%n sekundy</numerusform>
            <numerusform>%n sekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Notify about birthdays</source>
        <translation>Notify about birthdays</translation>
    </message>
    <message>
        <source>Notify about name-days</source>
        <translation>Notify about name-days</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>Ukázat nápovědu</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Import data from ext_info module</source>
        <translation>Zavést data z modulu ext_info</translation>
    </message>
    <message>
        <source>Birthday/name-day notification</source>
        <translation>Oznámení narozenin/jmenin</translation>
    </message>
</context>
<context>
    <name>@nextinfo</name>
    <message>
        <source>Extended information</source>
        <translation>Rozšířené informace</translation>
    </message>
    <message>
        <source>Birthday notification</source>
        <translation>Oznámení narozenin</translation>
    </message>
    <message>
        <source>Name-day notification</source>
        <translation>Oznámení jmenin</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Křestní jméno</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation>Pohlaví</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Přenosný telefon</translation>
    </message>
    <message>
        <source>female</source>
        <translation>Žena</translation>
    </message>
    <message>
        <source>male</source>
        <translation>Muž</translation>
    </message>
    <message>
        <source>e-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Alternative e-mail</source>
        <translation>Náhradní e-mail</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Stránky</translation>
    </message>
    <message>
        <source>Birthday (DD.MM.YYYY)</source>
        <translation>Narozeniny (DD.MM.RRRR)</translation>
    </message>
    <message>
        <source>Name-day (DD.MM)</source>
        <translation>Jmeniny (DD.MM)</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <source>Interests</source>
        <translation>Zájmy</translation>
    </message>
    <message>
        <source>These tags are recognised by the Kadu&apos;s parser:</source>
        <translation>tyto značky jsou rozpoznány syntaktickým analyzátorem Kadur:</translation>
    </message>
    <message>
        <source>Birthday notifications</source>
        <translation>Oznámení narozenin</translation>
    </message>
    <message>
        <source>Name-day notifications</source>
        <translation>Oznámení jmenin</translation>
    </message>
    <message>
        <source>Keep reminding me now</source>
        <translation>Připomínej mi nyní dokola</translation>
    </message>
    <message>
        <source>Remind me tomorrow</source>
        <translation>Připomeň mi to zítra</translation>
    </message>
    <message>
        <source>Remind me on the day</source>
        <translation>Připomeň mi to ten den</translation>
    </message>
    <message>
        <source>Remind me next year</source>
        <translation>Připomeň mi to další rok</translation>
    </message>
    <message>
        <source>code(n-th birthday)</source>
        <translatorcomment>JavaScript code returning localized ordinal number for n. In English translation: &apos;1st&apos; for n=1, &apos;2nd&apos; for n=2, etc.</translatorcomment>
        <translation>code(n-té narozeniny)</translation>
    </message>
    <message>
        <source>Select ext_info data file to import</source>
        <translation>Vybrat soubor s daty ext_info k zavedení</translation>
    </message>
    <message>
        <source>E-Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>IRC</source>
        <translation>IRC</translation>
    </message>
    <message>
        <source>Tlen</source>
        <translation>Tlen</translation>
    </message>
    <message>
        <source>WP</source>
        <translation>WP</translation>
    </message>
    <message>
        <source>ICQ</source>
        <translation>ICQ</translation>
    </message>
    <message numerus="yes">
        <source>%n contact(s) imported.</source>
        <translation>
            <numerusform>%n spojení zavedeno.</numerusform>
            <numerusform>%n spojení zavedena.</numerusform>
            <numerusform>%n spojení zavedeno.</numerusform>
        </translation>
    </message>
    <message>
        <source>Kadu</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;%2 narozeniny&lt;/b&gt; za &lt;b&gt;%3 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; za &lt;b&gt;%3 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;%2 birthday&lt;/b&gt; in &lt;b&gt;%3 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;%2 narozeniny&lt;/b&gt; za &lt;b&gt;%3 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;today&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;dnes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; &lt;b&gt;tomorrow&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; &lt;b&gt;zítra&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has their &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translatorcomment>singular they; see http://en.wikipedia.org/wiki/Singular_they</translatorcomment>
        <translation>&lt;b&gt;%1&lt;/b&gt; mají své &lt;b&gt;jmeniny&lt;/b&gt; za &lt;b&gt;%2 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has her &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; za &lt;b&gt;%2 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;%1&lt;/b&gt; has his &lt;b&gt;name-day&lt;/b&gt; in &lt;b&gt;%2 days&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; má své &lt;b&gt;jmeniny&lt;/b&gt; za &lt;b&gt;%2 dny&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Data import</source>
        <translation>Zavedení dat</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Narozeniny</translation>
    </message>
    <message>
        <source>Name-day</source>
        <translation>Jmeniny</translation>
    </message>
    <message>
        <source>Middle name</source>
        <translation>Druhé jméno</translation>
    </message>
</context>
<context>
    <name>ExtendedInformationWidget</name>
    <message>
        <source>Extended Information</source>
        <translation>Rozšířené informace</translation>
    </message>
</context>
<context>
    <name>NotesWidget</name>
    <message>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
</context>
</TS>
