/****************************************************************************
*                                                                           *
*   NExtInfo plugin for Kadu                                                *
*   Copyright (C) 2014  Piotr Dąbrowski ultr@ultr.pl                        *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/


#ifndef NOTES_WIDGET_FACTORY_H
	#define NOTES_WIDGET_FACTORY_H


#include "gui/widgets/buddy-configuration-widget-factory.h"


class NotesWidgetFactory : public BuddyConfigurationWidgetFactory
{
	public:
		virtual ~NotesWidgetFactory();
		virtual BuddyConfigurationWidget *createWidget(const Buddy &buddy, QWidget *parent);
};


#endif
