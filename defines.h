#define  NEXTINFO_DATAFORMATVERSION                                                         2
#define  NEXTINFO_DEFAULTNOTIFICATIONADVANCE                                                1  /* days */
#define  NEXTINFO_DEFAULTDELAYBETWEENNOTIFICATIONS                                      10*60  /* seconds */
#define  NEXTINFO_INITIALNOTIFYBIRTHDAYNAMEDAYINTERVAL                                     50  /* ms */
#define  NEXTINFO_LABELINFOMINIMUMWIDTH                                                   400  /* px */
#define  NEXTINFO_FIELDINTERESTSHEIGHT                                                     60  /* px */
#define  NEXTINFO_FIELDSEPARATORSHEIGHT                                                    10  /* px */
#define  NEXTINFO_REGEXPBIRTHDAY                       "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{4}$"  /* DD.MM.YYYY */
#define  NEXTINFO_REGEXPNAMEDAY                                   "^[0-9]{1,2}\\.[0-9]{1,2}$"  /* DD.MM */
#define  NEXTINFO_TABPOS_EXTINFO                                                            3
#define  NEXTINFO_TABPOS_NOTES                                                              4
