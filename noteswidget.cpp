/****************************************************************************
*                                                                           *
*   NExtInfo plugin for Kadu                                                *
*   Copyright (C) 2014  Piotr Dąbrowski ultr@ultr.pl                        *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/




#include "noteswidget.h"

#include <QCoreApplication>

#include "buddynextinfodata.h"




NotesWidget::NotesWidget(const Buddy &buddy, QWidget *parent) :
	BuddyConfigurationWidget( buddy, parent ),
	StateNotifier( new SimpleConfigurationValueStateNotifier( this ) )
{
	setWindowTitle( tr( "Notes" ) );
	createGui();
	configurationUpdated();
	loadValues();
}


NotesWidget::~NotesWidget()
{
}


void NotesWidget::createGui()
{
	layout_notestab = new QVBoxLayout( this );
	layout_notestab->setMargin( 5 );
	layout_notestab->setSpacing( 3 );

	notestab_separator1 = new QWidget( this );
		notestab_separator1->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		notestab_separator1->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_notestab->addWidget( notestab_separator1 );

	label_interests = new QLabel( this );
		layout_notestab->addWidget( label_interests );
		field_interests = new QTextEdit( this );
		field_interests->setMaximumSize( QSize( 32767, NEXTINFO_FIELDINTERESTSHEIGHT ) );
		field_interests->setTabChangesFocus( true );
		layout_notestab->addWidget( field_interests );

	notestab_separator2 = new QWidget( this );
		notestab_separator2->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		notestab_separator2->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_notestab->addWidget( notestab_separator2 );

	label_notes = new QLabel( this );
		layout_notestab->addWidget( label_notes );
		field_notes = new QTextEdit( this );
		field_notes->setTabChangesFocus( true );
		layout_notestab->addWidget( field_notes );

	notestab_separator3 = new QWidget( this );
		notestab_separator3->setMinimumHeight( NEXTINFO_FIELDSEPARATORSHEIGHT );
		notestab_separator3->resize( 1, NEXTINFO_FIELDSEPARATORSHEIGHT );
		layout_notestab->addWidget( notestab_separator3 );

	updateWidgets();
}


void NotesWidget::updateWidgets()
{
	// notes
	label_interests->setText( QCoreApplication::translate( "@nextinfo", "Interests" ) + ":" );
	label_notes->setText(     QCoreApplication::translate( "@nextinfo", "Notes"     ) + ":" );
}


void NotesWidget::configurationUpdated()
{
}


void NotesWidget::loadValues()
{
	// load extended information
	field_interests->setText(  BuddyNExtInfoData::interests( buddy() ) );
	field_notes->setText(      BuddyNExtInfoData::notes(     buddy() ) );
	// set state notifier
	StateNotifier->setState(StateNotChanged);
}


void NotesWidget::updateState()
{
	StateNotifier->setState( StateChangedDataValid );
}


const ConfigurationValueStateNotifier * NotesWidget::stateNotifier() const
{
	return StateNotifier;
}


void NotesWidget::apply()
{
	// save extended information
	BuddyNExtInfoData::setInterests( buddy(), field_interests->toPlainText() );
	BuddyNExtInfoData::setNotes(     buddy(), field_notes->toPlainText()     );
	// set state notifier
	StateNotifier->setState(StateNotChanged);
	// reload values later (ex. to update standard information)
	QMetaObject::invokeMethod( this, "loadValues", Qt::QueuedConnection );
}


void NotesWidget::cancel()
{
	loadValues();
}




#include "moc_noteswidget.cpp"
